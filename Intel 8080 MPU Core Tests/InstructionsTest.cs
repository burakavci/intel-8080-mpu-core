﻿using Intel_8080_MPU_Core.Models;
using Intel_8080_MPU_Core.Units;
using Intel_8080_MPU_Core.Units.MPU8080;
using System;
using Xunit;
using Xunit.Abstractions;

namespace Intel_8080_MPU_Core_Tests
{
    public class InstructionsTest
    {
        private readonly ITestOutputHelper output;
        MPU8080 intel;
        public InstructionsTest(ITestOutputHelper output)
        {
            intel = new MPU8080(rand_64Kb_Mem(), new BasicFlags(), 256, false, false);
            this.output = output;
        }
        IRAM rand_64Kb_Mem()
        {
            Random rnd = new Random();
            RAM ram = new RAM(64 * 1024);
            for (ulong i = 0; i < ram.Size; i++)
            {
                ram.WriteByte(i, (byte)rnd.Next(0, 255));
            }
            return ram;
        }
        [Fact]
        public void HalfCarryBitTest()
        {
            intel.Registers.Accumulator = 0x2E;
            intel.Registers.B = 0x74;
            intel.ADD(Registers.RegisterEncoding.B);
            Assert.True(intel.Registers.F.AC == 1);
        }
        [Fact]
        public void CarryBitTest()
        {
            intel.Registers.Accumulator = 0xFF;
            intel.Registers.B = 0xFF;
            intel.ADD(Registers.RegisterEncoding.B);
            Assert.True(intel.Registers.F.CY == 1);
        }
        [Fact]
        public void SignBitTest()
        {
            intel.Registers.Accumulator = 0xFE;
            intel.Registers.B = 0xFF;
            intel.SUB(Registers.RegisterEncoding.B);
            Assert.True(intel.Registers.F.S == 1);
        }
        [Fact]
        public void NegativeValTest()
        {
            intel.Registers.Accumulator = 0xFE;
            intel.Registers.B = 0xFF;
            intel.SUB(Registers.RegisterEncoding.B);
            Assert.Equal(-1, (sbyte)intel.Registers.Accumulator);
        }
        [Fact]
        public void LittleEndiannessTest()
        {
            byte[] b = { intel.RAM.ReadByte(1), intel.RAM.ReadByte(0) };
            byte[] litEndb = intel.EndianIO.ReadTwoBytes(EndianIO.Endianness.Little);
            Assert.Equal(b, litEndb);
        }
        [Fact]
        public void BigEndiannessTest()
        {
            byte[] b = { intel.RAM.ReadByte(0), intel.RAM.ReadByte(1) };
            byte[] bigEndb = intel.EndianIO.ReadTwoBytes(EndianIO.Endianness.Big);
            Assert.Equal(b, bigEndb);
        }
        [Fact]
        public void IncrementTest()
        {
            byte b = intel.Registers.C;
            intel.INR(Registers.RegisterEncoding.C);
            Assert.Equal(b, intel.Registers.C - 1);
        }
        [Fact]
        public void HalfCarryIncrementTrueTest()
        {
            intel.Registers.C = 0x0F;
            intel.INR(Registers.RegisterEncoding.C);
            Assert.True(intel.Registers.F.AC == 1);
        }
        [Fact]
        public void HalfCarryIncrementFalseTest()
        {
            intel.Registers.C = 0x0E;
            intel.INR(Registers.RegisterEncoding.C);
            Assert.False(intel.Registers.F.AC == 1);
        }
        [Fact]
        public void ReadUShortAtLittleTest()
        {
            IRAM ram = new RAM(12);
            ram.WriteByte(0, 0x12);
            ram.WriteByte(1, 0x73);
            Registers reg = new Registers(new BasicFlags());
            EndianIO eio = new EndianIO(ram, reg);
            ushort read = eio.ReadUShort(EndianIO.Endianness.Little);
            Assert.Equal(0x7312, read);
        }
        [Fact]
        public void PSHHLTest()
        {
            IRAM ram = new RAM(12);
            intel = new MPU8080(ram, new BasicFlags(), 256, false, false);
            intel.Registers.SP = 2;
            intel.Registers.HL = 0x7312;
            intel.PUSH(Registers.RegisterPairEncoding.HL);
            //intel.EndianIO.WriteUShort(0, intel.Registers.HL, EndianIO.Endianness.Little);
            output.WriteLine(BitConverter.ToString(new byte[] { ram.ReadByte(0), ram.ReadByte(1) }));
            Assert.Equal(0x7312, intel.EndianIO.ReadUShortAt(0, EndianIO.Endianness.Little));
        }
        [Fact]
        public void DAATest()
        {
            intel.Registers.A = 0x26;
            intel.Registers.B = 0x18;
            intel.ADD(Registers.RegisterEncoding.B);
            Assert.Equal(0x3E, intel.Registers.A);
            intel.DAA();
            Assert.Equal(0x44, intel.Registers.A);
        }
        [Fact]
        public void CMCTest()
        {
            intel.Registers.F.CY = 0;
            intel.CMC();
            Assert.Equal(1, intel.Registers.F.CY);
        }
        [Fact]
        public void STCTest()
        {
            intel.Registers.F.CY = 0;
            intel.STC();
            Assert.Equal(1, intel.Registers.F.CY);
        }
        [Fact]
        public void RLCTest()
        {
            intel.Registers.F.CY = 0;
            intel.Registers.A = 0xFE;
            intel.RLC();
            Assert.Equal(1, intel.Registers.F.CY);
            Assert.Equal(0xFD, intel.Registers.A);
        }
        [Fact]
        public void RRCTest()
        {
            intel.Registers.F.CY = 0;
            intel.Registers.A = 0xFD;
            intel.RRC();
            Assert.Equal(1, intel.Registers.F.CY);
            Assert.Equal(0xFE, intel.Registers.A);
        }
        [Fact]
        public void RARTest()
        {
            intel.Registers.F.CY = 1;
            intel.Registers.A = 0xFE;
            intel.RAR();
            Assert.Equal(0, intel.Registers.F.CY);
            Assert.Equal(0xFF, intel.Registers.A);
        }
        [Fact]
        public void RALTest()
        {
            intel.Registers.F.CY = 0;
            intel.Registers.A = 0xFE;
            intel.RAL();
            Assert.Equal(1, intel.Registers.F.CY);
            Assert.Equal(0xFC, intel.Registers.A);
        }
        [Fact]
        public void CMATest()
        {
            intel.Registers.A = 0x8F;
            intel.CMA();
            Assert.Equal(0x70, intel.Registers.A);
        }
        [Fact]
        public void CP_CompareTest()
        {
            intel.Registers.F.CY = intel.Registers.F.Z = 1;
            intel.Registers.A = 0xA;
            intel.Registers.E = 0x5;
            intel.CMP(Registers.RegisterEncoding.E);
            Assert.Equal(0, intel.Registers.F.CY);
            Assert.Equal(0, intel.Registers.F.Z);

            intel.Registers.F.CY = intel.Registers.F.Z = 1;
            intel.Registers.A = 0xE5;
            intel.Registers.E = 0x5;
            intel.CMP(Registers.RegisterEncoding.E);
            Assert.Equal(0, intel.Registers.F.CY);
            Assert.Equal(0, intel.Registers.F.Z);
        }
        [Fact]
        public void SBBTest()
        {
            intel.Registers.F.CY = 1;
            intel.Registers.E = 255;
            intel.Registers.A = 255;
            intel.SBB(Registers.RegisterEncoding.E);
            Assert.Equal(1, intel.Registers.F.CY);
            Assert.Equal(255, intel.Registers.A);

            intel.Registers.F.CY = 1;
            intel.Registers.E = 0;
            intel.Registers.A = 255;
            intel.SBB(Registers.RegisterEncoding.E);
            Assert.Equal(0, intel.Registers.F.CY);
            Assert.Equal(254, intel.Registers.A);
        }
    }
}
