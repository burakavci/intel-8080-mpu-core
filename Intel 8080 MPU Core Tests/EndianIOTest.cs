﻿using Intel_8080_MPU_Core.Units;
using Xunit;

namespace Intel_8080_MPU_Core_Tests
{
    public class EndianIOTest
    {
        public EndianIOTest()
        {
            RAM rm = new RAM(64 * 1024);
            rm.WriteByte(0xA00, 0x12);
            rm.WriteByte(0xA01, 0x73);
            reg = new Registers(new BasicFlags());
            generateTestIO = new EndianIO(rm, reg);
        }
        Registers reg;
        EndianIO generateTestIO;
        [Fact]
        public void ReadByte()
        {
            reg.PC = 0xA00;
            byte b = generateTestIO.ReadByte();
            Assert.Equal(0x12, b);
        }
        [Fact]
        public void ReadTwoBytesLittle()
        {
            reg.PC = 0xA00;
            byte[] b = generateTestIO.ReadTwoBytes(EndianIO.Endianness.Little);
            Assert.Equal(new byte[] { 0x73, 0x12 }, b);
        }
        [Fact]
        public void ReadTwoBytesBig()
        {
            reg.PC = 0xA00;
            byte[] b = generateTestIO.ReadTwoBytes(EndianIO.Endianness.Big);
            Assert.Equal(new byte[] { 0x12, 0x73 }, b);
        }
        [Fact]
        public void ReadUShortLittle()
        {
            reg.PC = 0xA00;
            ushort u = generateTestIO.ReadUShort(EndianIO.Endianness.Little);
            Assert.Equal(29458, u);
        }
        [Fact]
        public void ReadUShortBig()
        {
            reg.PC = 0xA00;
            ushort u = generateTestIO.ReadUShort(EndianIO.Endianness.Big);
            Assert.Equal(4723, u);
        }
        [Fact]
        public void ReadByteAt()
        {
            byte b = generateTestIO.ReadByteAt(0xA01);
            Assert.Equal(0x73, b);
        }
        [Fact]
        public void ReadTwoBytesAtLittle()
        {
            byte[] b = generateTestIO.ReadTwoBytesAt(0xA00, EndianIO.Endianness.Little);
            Assert.Equal(new byte[] { 0x73, 0x12 }, b);
        }
        [Fact]
        public void ReadTwoBytesAtBig()
        {
            byte[] b = generateTestIO.ReadTwoBytesAt(0xA00, EndianIO.Endianness.Big);
            Assert.Equal(new byte[] { 0x12, 0x73 }, b);
        }
        [Fact]
        public void ReadUShortAtLittle()
        {
            ushort u = generateTestIO.ReadUShortAt(0xA00, EndianIO.Endianness.Little);
            Assert.Equal(29458, u);
        }
        [Fact]
        public void ReadUshortAtBig()
        {
            ushort u = generateTestIO.ReadUShortAt(0xA00, EndianIO.Endianness.Big);
            Assert.Equal(4723, u);
        }
        [Fact]
        public void WriteByte()
        {
            reg.PC = 0xA00;
            generateTestIO.WriteByte(0xA00, 0x8F);
            Assert.Equal(0x8F, generateTestIO.ReadByteAt(0xA00));
            Assert.Equal(0x8F, generateTestIO.ReadByte());
        }
        [Fact]
        public void WriteTwoBytesLittle()
        {
            reg.PC = 0xA00;
            generateTestIO.WriteTwoBytes(0xA00, new byte[] { 0x48, 0x33 }, EndianIO.Endianness.Little);
            Assert.Equal(new byte[] { 0x48, 0x33 }, generateTestIO.ReadTwoBytes(EndianIO.Endianness.Little));
        }
        [Fact]
        public void WriteTwoBytesBig()
        {
            reg.PC = 0xA00;
            generateTestIO.WriteTwoBytes(0xA00, new byte[] { 0x48, 0x33 }, EndianIO.Endianness.Big);
            Assert.Equal(new byte[] { 0x48, 0x33 }, generateTestIO.ReadTwoBytes(EndianIO.Endianness.Big));
        }
        [Fact]
        public void WriteUShortLittle()
        {
            reg.PC = 0xA00;
            generateTestIO.WriteUShort(0xA00, 0x4833, EndianIO.Endianness.Little);
            Assert.Equal(0x4833, generateTestIO.ReadUShort(EndianIO.Endianness.Little));
        }
        [Fact]
        public void WriteUShortBig()
        {
            reg.PC = 0xA00;
            generateTestIO.WriteUShort(0xA00, 0x4833, EndianIO.Endianness.Big);
            Assert.Equal(0x4833, generateTestIO.ReadUShort(EndianIO.Endianness.Big));
        }
    }
}
