﻿using Intel_8080_MPU_Core.Models;

namespace Intel_8080_MPU_Core.Units
{
    public class Registers
    {
        public enum RegisterEncoding
        {
            A = 7,
            B = 0,
            C = 1,
            D = 2,
            E = 3,
            H = 4,
            L = 5,
            None = 8
        }
        public enum RegisterPairEncoding
        {
            BC = 0,
            DE = 1,
            HL = 2,
            SP = 3,
            None = 4
        }
        ushort buffer;
        public byte A, B, C, D, E, H, L;
        public IFlags F;
        public ushort PC, SP;
        public ushort ProgramCounter { get { return PC; } set { PC = value; } }
        public ushort StackPointer { get { return SP; } set { SP = value; } }
        public byte Accumulator { get { return A; } set { A = value; } }
        public ushort BC
        {
            get
            {
                buffer = 0;
                buffer |= B;
                buffer <<= 8;
                buffer |= C;
                return buffer;
            }
            set
            {
                C = (byte)(0xFF & value);
                value >>= 8;
                B = (byte)value;
            }
        }
        public ushort DE
        {
            get
            {
                buffer = 0;
                buffer |= D;
                buffer <<= 8;
                buffer |= E;
                return buffer;
            }
            set
            {
                E = (byte)(0xFF & value);
                value >>= 8;
                D = (byte)value;
            }
        }
        public ushort HL
        {
            get
            {
                buffer = 0;
                buffer |= H;
                buffer <<= 8;
                buffer |= L;
                return buffer;
            }
            set
            {
                L = (byte)(0xFF & value);
                value >>= 8;
                H = (byte)value;
            }
        }
        public Registers(IFlags flags)
        {
            F = flags;
        }

        public void SetRegister(RegisterEncoding register, byte data)
        {
            switch (register)
            {
                case RegisterEncoding.A:
                    A = data;
                    return;
                case RegisterEncoding.B:
                    B = data;
                    return;
                case RegisterEncoding.C:
                    C = data;
                    return;
                case RegisterEncoding.D:
                    D = data;
                    return;
                case RegisterEncoding.E:
                    E = data;
                    return;
                case RegisterEncoding.H:
                    H = data;
                    return;
                case RegisterEncoding.L:
                    L = data;
                    return;
                default:
                    throw new System.Exception("No Register Encoding");
            }
        }
        public byte GetRegister(RegisterEncoding register)
        {
            switch (register)
            {
                case RegisterEncoding.A:
                    return A;
                case RegisterEncoding.B:
                    return B;
                case RegisterEncoding.C:
                    return C;
                case RegisterEncoding.D:
                    return D;
                case RegisterEncoding.E:
                    return E;
                case RegisterEncoding.H:
                    return H;
                case RegisterEncoding.L:
                    return L;
                default:
                    throw new System.Exception("No Register Encoding");
            }
        }
        public void IncreaseRegister(RegisterEncoding register)
        {
            switch (register)
            {
                case RegisterEncoding.A:
                    A++;
                    return;
                case RegisterEncoding.B:
                    B++;
                    return;
                case RegisterEncoding.C:
                    C++;
                    return;
                case RegisterEncoding.D:
                    D++;
                    return;
                case RegisterEncoding.E:
                    E++;
                    return;
                case RegisterEncoding.H:
                    H++;
                    return;
                case RegisterEncoding.L:
                    L++;
                    return;
                default:
                    throw new System.Exception("No Register Encoding");
            }
        }
        public void DecreaseRegister(RegisterEncoding register)
        {
            switch (register)
            {
                case RegisterEncoding.A:
                    A--;
                    return;
                case RegisterEncoding.B:
                    B--;
                    return;
                case RegisterEncoding.C:
                    C--;
                    return;
                case RegisterEncoding.D:
                    D--;
                    return;
                case RegisterEncoding.E:
                    E--;
                    return;
                case RegisterEncoding.H:
                    H--;
                    return;
                case RegisterEncoding.L:
                    L--;
                    return;
                default:
                    throw new System.Exception("No Register Encoding");
            }
        }
        public void SetRegisterPair(RegisterPairEncoding registerPair, ushort data)
        {
            switch (registerPair)
            {
                case RegisterPairEncoding.BC:
                    BC = data;
                    return;
                case RegisterPairEncoding.DE:
                    DE = data;
                    return;
                case RegisterPairEncoding.HL:
                    HL = data;
                    return;
                case RegisterPairEncoding.SP:
                    SP = data;
                    return;
                default:
                    throw new System.Exception("No Register Pair Encoding");
            }
        }
        public void IncreaseRegisterPair(RegisterPairEncoding registerPair)
        {
            switch (registerPair)
            {
                case RegisterPairEncoding.BC:
                    BC++;
                    return;
                case RegisterPairEncoding.DE:
                    DE++;
                    return;
                case RegisterPairEncoding.HL:
                    HL++;
                    return;
                case RegisterPairEncoding.SP:
                    SP++;
                    return;
                default:
                    throw new System.Exception("No Register Pair Encoding");
            }
        }
        public void DecreaseRegisterPair(RegisterPairEncoding registerPair)
        {
            switch (registerPair)
            {
                case RegisterPairEncoding.BC:
                    BC--;
                    return;
                case RegisterPairEncoding.DE:
                    DE--;
                    return;
                case RegisterPairEncoding.HL:
                    HL--;
                    return;
                case RegisterPairEncoding.SP:
                    SP--;
                    return;
                default:
                    throw new System.Exception("No Register Pair Encoding");
            }
        }
        public ushort GetRegisterPair(RegisterPairEncoding registerPair)
        {
            switch (registerPair)
            {
                case RegisterPairEncoding.BC:
                    return BC;
                case RegisterPairEncoding.DE:
                    return DE;
                case RegisterPairEncoding.HL:
                    return HL;
                case RegisterPairEncoding.SP:
                    return SP;
                default:
                    throw new System.Exception("No Register Pair Encoding");
            }
        }
    }
}
