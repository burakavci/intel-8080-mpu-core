﻿using Intel_8080_MPU_Core.Models;
using System;

namespace Intel_8080_MPU_Core.Units
{
    public class BasicFlags : IFlags
    {
        public enum Masks
        {
            S = 0x80,
            Z = 0x40,
            AC = 0x10,
            P = 0x4,
            CY = 0x1
        }
        public enum Indexes
        {
            S = 7,
            Z = 6,
            AC = 4,
            P = 2,
            CY = 0
        }
        public static byte F_Default = 0b00000010;
        public byte Flags
        {
            get
            {
                if ((flags & 0x28) != 0 || (flags & 2) != 2)
                    throw new Exception("flag error");
                return flags;
            }
            set
            {
                flags = value;
                flags |= 1 << 1;
                flags &= 0xD7;
            }
        }
        byte flags = 2;
        public byte S
        {
            get
            {
                return FlagToByte(7);
            }
            set
            {
                if (value > 1)
                    throw new Exception("Flag higher than 1");
                Bit(7, value == 1);
            }
        }
        public byte Z
        {
            get
            {
                return FlagToByte(6);
            }
            set
            {
                if (value > 1)
                    throw new Exception("Flag higher than 1");
                Bit(6, value == 1);
            }
        }
        public byte AC { get { return FlagToByte(4); } set { if (value > 1) throw new Exception("Flag higher than 1"); Bit(4, value == 1); } }
        public byte P { get { return FlagToByte(2); } set { if (value > 1) throw new Exception("Flag higher than 1"); Bit(2, value == 1); } }
        public byte CY { get { return FlagToByte(0); } set { if (value > 1) throw new Exception("Flag higher than 1"); Bit(0, value == 1); } }
        void Bit(byte index, bool value)
        {
            if (value)
                Flags |= (byte)(1 << index);
            else
                Flags &= (byte)~(1 << index);
        }
        bool GetBit(byte index)
        {
            return (Flags & 1 << index) >> index == 1;
        }
        byte FlagToByte(byte index)
        {
            return (byte)((Flags & 1 << index) >> index);
        }
        public void Parity(byte result)
        {
            P = 1;
            for (byte i = 0; i < 8; i++)
            {
                P ^= (byte)((result & 1 << i) >> i);
            }
        }
    }
}
