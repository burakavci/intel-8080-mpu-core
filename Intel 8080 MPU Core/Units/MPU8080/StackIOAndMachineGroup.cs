﻿namespace Intel_8080_MPU_Core.Units.MPU8080
{
    public partial class MPU8080
    {
        public int PUSH(Registers.RegisterPairEncoding registerPair)
        {
            EndianIO.WriteUShort((ushort)(Registers.SP - 2), Registers.GetRegisterPair(registerPair), EndianIO.Endianness.Little);
            Registers.SP -= 2;
            return 11;
        }
        public int PUSH()
        {
            EndianIO.WriteByte((ushort)(Registers.SP - 1), Registers.A);
            EndianIO.WriteByte((ushort)(Registers.SP - 2), Registers.F.Flags);
            Registers.SP -= 2;
            return 11;
        }
        public int POP(Registers.RegisterPairEncoding registerPair)
        {
            Registers.SetRegisterPair(registerPair, EndianIO.ReadUShortAt(Registers.SP, EndianIO.Endianness.Little));
            Registers.SP += 2;
            return 10;
        }
        public int POP()
        {
            Registers.F.Flags = EndianIO.ReadByteAt(Registers.SP);
            Registers.A = EndianIO.ReadByteAt((ushort)(Registers.SP + 1));
            Registers.SP += 2;
            return 10;
        }
        public int XTHL()
        {
            byte temp = EndianIO.ReadByteAt(Registers.SP);
            EndianIO.WriteByte(Registers.SP, Registers.L);
            Registers.L = temp;
            temp = EndianIO.ReadByteAt((ushort)(Registers.SP + 1));
            EndianIO.WriteByte((ushort)(Registers.SP + 1), Registers.H);
            Registers.H = temp;
            return 18;
        }
        public int SPHL()
        {
            Registers.SP = Registers.HL;
            return 5;
        }
        public int IN()
        {
            Registers.A = Ports[EndianIO.ReadByte()];
            return 10;
        }
        public int OUT()
        {
            Ports[EndianIO.ReadByte()] = Registers.A;
            return 10;
        }
        public int EI()
        {
            interrupts = true;
            return 4;
        }
        public int DI()
        {
            interrupts = false;
            return 4;
        }
        public int HLT()
        {
            isHalted = true;
            return 7;
        }
        public int NOP()
        {
            return 4;
        }
    }
}
