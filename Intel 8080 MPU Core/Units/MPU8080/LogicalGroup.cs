﻿namespace Intel_8080_MPU_Core.Units.MPU8080
{
    public partial class MPU8080
    {
        public int ANA(Registers.RegisterEncoding source)
        {
            byte temp = Registers.GetRegister(source);
            Registers.F.AC = GetBit((byte)(Registers.A | temp), 3);
            AndAccumulator(temp);
            return 4;
        }
        public int ANA()
        {
            byte temp = EndianIO.ReadByteAt(Registers.HL);
            Registers.F.AC = GetBit((byte)(Registers.A | temp), 3);
            AndAccumulator(temp);
            return 7;
        }
        public int ANI()
        {
            byte temp = EndianIO.ReadByte();
            Registers.F.AC = GetBit((byte)(Registers.A | temp), 3);
            AndAccumulator(temp);
            return 7;
        }
        public int XRA(Registers.RegisterEncoding source)
        {
            XOrAccumulator(Registers.GetRegister(source));
            return 4;
        }
        public int XRA()
        {
            XOrAccumulator(EndianIO.ReadByteAt(Registers.HL));
            return 7;
        }
        public int XRI()
        {
            XOrAccumulator(EndianIO.ReadByte());
            return 7;
        }
        public int ORA(Registers.RegisterEncoding source)
        {
            OrAccumulator(Registers.GetRegister(source));
            return 4;
        }
        public int ORA()
        {
            OrAccumulator(EndianIO.ReadByteAt(Registers.HL));
            return 7;
        }
        public int ORI()
        {
            OrAccumulator(EndianIO.ReadByte());
            return 7;
        }
        public int CMP(Registers.RegisterEncoding source)
        {
            CompareAccumulator(Registers.GetRegister(source));
            return 4;
        }
        public int CMP()
        {
            CompareAccumulator(EndianIO.ReadByteAt(Registers.HL));
            return 7;
        }
        public int CPI()
        {
            CompareAccumulator(EndianIO.ReadByte());
            return 7;
        }
        public int RLC()
        {
            Registers.F.CY = GetBit(Registers.A, 7);
            Registers.A = (byte)((Registers.A << 1) | Registers.F.CY);
            return 4;
        }
        public int RRC()
        {
            Registers.F.CY = GetBit(Registers.A, 0);
            Registers.A = (byte)((Registers.A >> 1) | (Registers.F.CY << 7));
            return 4;
        }
        public int RAL()
        {
            byte prevCY = Registers.F.CY;
            Registers.F.CY = GetBit(Registers.A, 7);
            Registers.A = (byte)((Registers.A << 1) | prevCY);
            return 4;
        }
        public int RAR()
        {
            byte prevCY = Registers.F.CY;
            Registers.F.CY = GetBit(Registers.A, 0);
            Registers.A = (byte)((Registers.A >> 1) | (prevCY << 7));
            return 4;
        }
        public int CMA()
        {
            Registers.A ^= 0xFF;
            return 4;
        }
        public int CMC()
        {
            Registers.F.CY ^= 1;
            return 4;
        }
        public int STC()
        {
            Registers.F.CY = 1;
            return 4;
        }

        void AndAccumulator(byte data)
        {
            Registers.F.CY = 0;
            Registers.A &= data;
            ZeroSignParity(Registers.A);
        }
        void XOrAccumulator(byte data)
        {
            Registers.F.AC = 0;
            Registers.F.CY = 0;
            Registers.A ^= data;
            ZeroSignParity(Registers.A);
        }
        void OrAccumulator(byte data)
        {
            Registers.F.AC = 0;
            Registers.F.CY = 0;
            Registers.A |= data;
            ZeroSignParity(Registers.A);
        }
        void CompareAccumulator(byte data)
        {
            Registers.F.CY = Registers.A < data ? (byte)1 : (byte)0;
            Registers.F.AC = (Registers.A & 0xF) < (data & 0xF) ? (byte)0 : (byte)1;
            data = ChangeSign(data);
            ZeroSignParity((byte)(Registers.A + data));
        }
    }
}
