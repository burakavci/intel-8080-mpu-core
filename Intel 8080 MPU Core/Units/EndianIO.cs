﻿using Intel_8080_MPU_Core.Models;

namespace Intel_8080_MPU_Core.Units
{
    public class EndianIO
    {
        IRAM RAM;
        Registers Registers;
        public enum Endianness
        {
            Big = 0,
            Little = 1
        }
        public EndianIO(IRAM ram, Registers registers)
        {
            RAM = ram;
            Registers = registers;
        }
        public byte ReadByte()
        {
            return RAM.ReadByte(Registers.PC++);
        }
        public byte[] ReadTwoBytes(Endianness endianness)
        {
            Registers.PC += 2;
            return ReadTwoBytesAt((ushort)(Registers.PC - 2), endianness);
        }
        public ushort ReadUShort(Endianness endianness)
        {
            Registers.PC += 2;
            return ReadUShortAt((ushort)(Registers.PC - 2), endianness);
        }
        public byte ReadByteAt(ushort address)
        {
            return RAM.ReadByte(address);
        }
        public ushort ReadUShortAt(ushort address, Endianness endianness)
        {
            if (endianness == Endianness.Big)
            {
                return (ushort)(((0x0000 | RAM.ReadByte(address)) << 8) | RAM.ReadByte((ushort)(address + 1)));
            }
            else
            {
                ushort bigEnd = ReadUShortAt(address, Endianness.Big);
                return (ushort)((bigEnd << 8) | (bigEnd >> 8));
            }
        }
        public byte[] ReadTwoBytesAt(ushort address, Endianness endianness)
        {
            if (endianness == Endianness.Big)
            {
                return new byte[] { RAM.ReadByte(address), RAM.ReadByte((ushort)(address + 1)) };
            }
            else
            {
                byte[] bigEnd = ReadTwoBytesAt(address, Endianness.Big);
                return new byte[] { bigEnd[1], bigEnd[0] };
            }
        }
        public void WriteByte(ushort address, byte data)
        {
            RAM.WriteByte(address, data);
        }
        public void WriteUShort(ushort address, ushort data, Endianness endianness)
        {
            if (endianness == Endianness.Big)
            {
                RAM.WriteByte(address, (byte)(data >> 8));
                RAM.WriteByte((ushort)(address + 1), (byte)(data & 0x00FF));
            }
            else
            {
                address++;
                RAM.WriteByte(address, (byte)(data >> 8));
                RAM.WriteByte((ushort)(address - 1), (byte)(data & 0x00FF));
            }
        }
        public void WriteTwoBytes(ushort address, byte[] data, Endianness endianness)
        {
            if (endianness == Endianness.Big)
            {
                RAM.WriteByte(address, data[0]);
                RAM.WriteByte((ushort)(address + 1), data[1]);
            }
            else
            {
                address++;
                RAM.WriteByte(address, data[0]);
                RAM.WriteByte((ushort)(address - 1), data[1]);
            }
        }
    }
}
