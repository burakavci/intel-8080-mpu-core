﻿using Intel_8080_MPU_Core.Models;

namespace Intel_8080_MPU_Core.Units
{
    public class Bootloader
    {
        public void LoadProgram(IRAM RAM, ushort startAddress, byte[] program)
        {
            for (ushort i = startAddress; i < startAddress + program.Length; i++)
            {
                RAM.WriteByte(i, program[i - startAddress]);
            }
        }
    }
}
