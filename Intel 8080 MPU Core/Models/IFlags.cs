﻿namespace Intel_8080_MPU_Core.Models
{
    public interface IFlags
    {
        byte S { get; set; }
        byte Z { get; set; }
        byte AC { get; set; }
        byte P { get; set; }
        byte CY { get; set; }
        byte Flags { get; set; }
        void Parity(byte result);
    }
}
