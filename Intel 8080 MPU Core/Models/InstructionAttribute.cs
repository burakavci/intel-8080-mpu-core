﻿using System;

namespace Intel_8080_MPU_Core.Models
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class InstructionAttribute : Attribute
    {
        [Flags]
        public enum Flags
        {
            None = 0,
            S = 1,
            Z = 2,
            A = 4,
            P = 8,
            C = 16,
            All = 32
        }
        Flags affectedFlags;
        byte length, duration;

        public Flags AffectedFlags { get => affectedFlags; }
        public byte Length { get => length; }
        public byte Duration { get => duration; }

        public InstructionAttribute(byte Length, byte Duration, Flags AffectedFlags)
        {
            this.length = Length;
            this.duration = Duration;
            this.affectedFlags = AffectedFlags;
        }
    }
}
