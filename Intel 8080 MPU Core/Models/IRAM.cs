﻿namespace Intel_8080_MPU_Core.Models
{
    public interface IRAM
    {
        ulong Size { get; set; }
        byte ReadByte(ushort address);
        byte ReadByte(ulong address);
        byte ReadByte(uint address);
        //short ReadShort(short address);
        //byte[] ReadTwoBytes(short address);
        void WriteByte(ushort address, byte data);
        void WriteByte(ulong address, byte data);
        void WriteByte(uint address, byte data);
        //void WriteShort(short address, short data);
        //void WriteTwoBytes(short address, byte[] data);
    }
}
