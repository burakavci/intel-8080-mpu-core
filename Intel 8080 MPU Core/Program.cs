﻿using Intel_8080_MPU_Core.Models;
using Intel_8080_MPU_Core.Units;
using Intel_8080_MPU_Core.Units.MPU8080;
using System.IO;

namespace Intel_8080_MPU_Core
{
    class Program
    {
        static void Main(string[] args)
        {
            IRAM ram = new RAM(64 * 1024);
            Bootloader bl = new Bootloader();
            string instructionExcerciser = "8080EX1.COM";
            string correctInstructionExcerciser = "8080EXM.COM";
            string preliminaryTest = "8080PRE.COM";
            string pretty = "pretty_8080exr.com";
            string cpuDiag = "cpudiag.bin";
            string cpuTest = "CPUTEST.COM";
            string tst8080 = "TST8080.COM";
            bl.LoadProgram(ram, 0x100, File.ReadAllBytes(correctInstructionExcerciser));
            MPU8080 cpu = new MPU8080(ram, new BasicFlags(), 256, true, true);
            cpu.Init(ulong.MaxValue);
        }
    }
}
